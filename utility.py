__author__ = ["Ahmed Mkadem"]
__maintainers__ = ["Ahmed Mkadem"]
__emails__ = ["Ahmed.Mkadem@xpo.com"]

__license__ = "GPL"

from datetime import datetime, timedelta
import requests
import json
import jwt
import uuid
import os
import re
from pymongo import MongoClient
import time
import csv
import json
import re

client = MongoClient() #if needed at the ('localhost', port) for the instance of mongo
database = client.cylancetest
collection= database.devicelist
#collection = collection

baseURL = 'https://protectapi.cylance.com'
creds = {
    "tenant": "",
    "app_id": "",
    "app_secret": "",
    "region": ""
}
auth_token = ""
device_headers = {"Content-Type": "application/json; charset=utf-8", "Authorization": "Bearer " + str(auth_token)}
timeout_datetime = datetime.utcnow()
timeout_epoch = ""

def load_creds(file):
    # get saved creds if they exist
    global creds
    if file == "":
        file = 'auth_config.json'
    with open(file) as infile:
        creds = json.load(infile)
    

def get_token():
    global creds, auth_token, device_headers
    global timeout_datetime
    load_creds('auth_config.json')
    #timeout = 1800  # 30 minutes from now
    now = datetime.utcnow()
    timeout_datetime = now + timedelta(minutes=30)
    epoch_time = int((now - datetime(1970, 1, 1)).total_seconds())
    epoch_timeout = int((timeout_datetime - datetime(1970, 1, 1)).total_seconds())
    jti_val = str(uuid.uuid4())
    tid_val = creds['tenant']
    app_id = creds['app_id']
    app_secret = creds['app_secret']
    claims = {
        "exp": epoch_timeout,
        "iat": epoch_time,
        "iss": "http://cylance.com",
        "sub": app_id,
        "tid": tid_val,
        "jti": jti_val
    }
    encoded = str(jwt.encode(claims, app_secret, algorithm='HS256'))[2:-1]
    payload = {"auth_token": encoded}
    headers = {"Content-Type": "application/json; charset=utf-8"}
    resp = requests.post(baseURL + '/auth/v2/token', headers=headers, data=json.dumps(payload))
    auth_token = json.loads(resp.text)['access_token']
    device_headers = {"Content-Type": "application/json; charset=utf-8", "Authorization": "Bearer " + str(auth_token)}
    return auth_token


def updateDevice_policy_zone(device_name, device_id, zone_id, policy_id):
    global timeout_datetime
    global auth_token
    requestData = {
        'name':device_name,
        'policy_id':policy_id,
        'add_zone_ids':[zone_id]
    }
    url = baseURL + '/devices/v2/'+device_id
    
    try:
        if (timeout_datetime  < datetime.utcnow() ):  # need to auth/re-auth
            auth_token = get_token()
            timeout_datetime = datetime.utcnow()+timedelta(minutes=30)
        r = requests.put(url , data=json.dumps(requestData), headers=device_headers)
        return r.status_code
    except Exception as e :
        print("Updating device "+device_name+" failed")
        print("Exception: "+str(e)) 


def updateDevice_policy(device_name,device_id, policy_id):
    global timeout_datetime
    global auth_token
    requestData = {
        'name': device_name,
        'policy_id': policy_id
    }
    url=baseURL + '/devices/v2/'+str(device_id)
    try:
        if (timeout_datetime  < datetime.utcnow() ):  # need to auth/re-auth
            auth_token = get_token()
            timeout_datetime = datetime.utcnow()+timedelta(minutes=30)
        r = requests.put(url , data=json.dumps(requestData), headers=device_headers)
        return r.status_code
    except Exception as e :
        print("Updating device "+device_name+" failed")
        print("Exception: "+str(e))     

def updateDevice_zone(device_name, device_id, zone_id):
    global auth_token
    global timeout_datetime
    requestData = {
        'name':device_name,
        'add_zone_ids':[zone_id]
    }
    url = baseURL + '/devices/v2/' + device_id
    try:
        if (timeout_datetime  < datetime.utcnow() ):  # need to auth/re-auth
            auth_token = get_token()
            timeout_datetime = datetime.utcnow()+timedelta(minutes=30)
        r = requests.put(url , data=json.dumps(requestData), headers=device_headers)
        return r.status_code
    except Exception as err:
        print("Updating device "+device_name+" failed")
        print("Exception: "+str(err))        

def getDeviceId(mac):
    global auth_token
    global timeout_datetime
    url = baseURL + '/devices/v2/macaddress/'+mac
    try:
        if (timeout_datetime  < datetime.utcnow() ):  # need to auth/re-auth
            auth_token = get_token()
            timeout_datetime = datetime.utcnow()+timedelta(minutes=30)
        r = requests.get(url , headers=device_headers)
        if len(r.json())>0:
            return [r.status_code,r.json()[0]['id']]
        return [r.status_code,'']
    except ValueError as err:
        print('Getting device id has been failed '+str(err))

def updatePolicyFromCSV(input_file,policy_id='0765bc5a-ff6e-4b5b-8153-26bc5dd7f880'):
    
    failed=[]
    with open(input_file,mode='r') as csv_file:
        csv_reader=csv.reader(csv_file,delimiter=',')
        line_counter=0
        for row in csv_reader:
            if line_counter==0:
                failed.append(row)
                name_index=row.index('Hostname')
                line_counter=1
                continue
            hostname=row[name_index]
            device_id=getIDfromDB(hostname)
            if len(device_id)>0:
                status_code=updateDevice_policy(hostname,device_id,policy_id)
                if status_code!=200:
                    row.append('status_code= '+str(status_code))
                    failed.append(row)
            else:
                row.append('ID NOT FOUND')
                failed.append(row)
    with open('failed.csv', mode='w') as csv_file:
        csv_writer=csv.writer(csv_file)
        csv_writer.writerows(failed)

    print('Failed update in failed.csv')  

def updateZoneFromCSV(input_file,zone_id='fa6c11d6-7ddc-4d7b-bfcf-6db750902f8d'):
    
    failed=[]
    with open(input_file,mode='r') as csv_file:
        csv_reader=csv.reader(csv_file,delimiter=',')
        line_counter=0
        for row in csv_reader:
            if line_counter==0:
                failed.append(row)
                name_index=row.index('Hostname')
                line_counter=1
                continue
            hostname=row[name_index]
            device_id=getIDfromDB(hostname)
            if len(device_id)>0:
                status_code=updateDevice_zone(hostname,device_id,zone_id)
                if status_code!=200:
                    row.append('status_code= '+str(status_code))
                    failed.append(row)
            else:
                row.append('ID NOT FOUND')
                failed.append(row)
    with open('failed.csv', mode='w') as csv_file:
        csv_writer=csv.writer(csv_file)
        csv_writer.writerows(failed)

    print(' Failed update in failed.csv')  
 

def updateZonePolicyFromCSV(input_file,zone_id='fa6c11d6-7ddc-4d7b-bfcf-6db750902f8d',policy_id='0765bc5a-ff6e-4b5b-8153-26bc5dd7f880'):
 
    failed=[]
    with open(input_file,mode='r') as csv_file:
        csv_reader=csv.reader(csv_file,delimiter=',')
        line_counter=0
        for row in csv_reader:
            if line_counter==0:
                failed.append(row)
                name_index=row.index('Hostname')
                line_counter=1
                continue
            hostname=row[name_index]
            device_id=getIDfromDB(hostname)
            if len(device_id)>0:
                status_code=updateDevice_policy_zone(hostname,device_id,zone_id,policy_id)
                if status_code!=200:
                    row.append('status_code= '+str(status_code))
                    failed.append(row)
            else:
                row.append('ID NOT FOUND')
                failed.append(row)

    with open('failed.csv', mode='w') as csv_file:
        csv_writer=csv.writer(csv_file)
        csv_writer.writerows(failed)

    print('Failed update in failed.csv')  

def updatePolicyFromJSON(input_file,policy_id='0765bc5a-ff6e-4b5b-8153-26bc5dd7f880'):
    failed=[]
    with open(input_file) as json_file:
        data=json.load(json_file)
        for element in data:
            device_id=getIDfromDB(element['Hostname'])
            if len(device_id)>0:
                status_code=updateDevice_policy(element['Hostname'],device_id,policy_id)
                if status_code!=200:
                    failed.append([element['Hostname'],element['MAC Address'],'status_code= '+str(status_code)])
            else:
                failed.append([element['Hostname'],element['MAC Address'],'ID NOT FOUND'])

    with open('failed.csv', mode='w') as csv_file:
        csv_writer=csv.writer(csv_file)
        csv_writer.writerows(failed)

    print('Failed update in failed.csv')  

def updateZoneFromJSON(input_file,zone_id='fa6c11d6-7ddc-4d7b-bfcf-6db750902f8d'):
    failed=[]
    success=[]
    with open(input_file) as json_file:
        data=json.load(json_file)
        for element in data:
            device_id=getIDfromDB(element['Hostname'])
            if len(device_id)>0:
                status_code=updateDevice_zone(element['Hostname'],device_id,zone_id)
                if status_code!=200:
                    failed.append([element['Hostname'],element['MAC Address'],'status_code= '+str(status_code)])
            else:
                failed.append([element['Hostname'],element['MAC Address'],'ID NOT FOUND'])

    with open(input_file+'failed.csv', mode='w') as csv_file:
        csv_writer=csv.writer(csv_file)
        csv_writer.writerows(failed)

    print('Failed update in failed.csv')  
 

def updateZonePolicyFromJSON(input_file,zone_id='fa6c11d6-7ddc-4d7b-bfcf-6db750902f8d',policy_id='0765bc5a-ff6e-4b5b-8153-26bc5dd7f880'):

    failed=[]
    with open(input_file) as json_file:
        data=json.load(json_file)
        for element in data:
            device_id=getIDfromDB(element['Hostname'])
            if len(device_id)>0:
                status_code=updateDevice_policy_zone(element['Hostname'], device_id, zone_id, policy_id)
                if status_code!=200:
                    failed.append([element['Hostname'],element['MAC Address'],'status_code= '+str(status_code)])
            else:
                failed.append([element['Hostname'],element['MAC Address'],'ID NOT FOUND'])

    with open('failed.csv', mode='w') as csv_file:
        csv_writer=csv.writer(csv_file)
        csv_writer.writerows(failed)

    print('Failed update in failed.csv')  

def getIDfromDB(hostname):
    deviceinfo = list(collection.find({'info.name':re.compile(hostname,re.IGNORECASE)}))
    if len(deviceinfo)>0:
        return deviceinfo[0]['info']['id']
    else:
        return ''



load_creds('auth_config.json')
get_token()        

