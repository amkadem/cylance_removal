import csv
import sys
import utility

zone_id=''
policy_id=''
device_id=''

if(len(sys.argv)!=2):
    print('Usage: python3 frontgun.py input_file\n')
    exit(1)

input_file=sys.argv[1]
file_type=input_file.split('.')[1]
if file_type not in ['csv','json']:
    print('Unknow file format\n')
    exit(1)
print('Reading data from '+file_type+' file\n')
selector =10

while(selector<1 or selector>3):
    print("Select option from menu\n")
    print("[+] Type 1 to change device policies\n")
    print("[+] Type 2 to change device zones\n")
    print("[+] Type 3 to change device  zones and policies\n")
    try:
        selector=int(input())
    except:
        continue

if(selector==1):
    if(file_type=='csv'):
        utility.updatePolicyFromCSV(input_file)
    else:
        utility.updatePolicyFromJSON(input_file)
elif(selector==2):
    zone_id=input("Enter zone ID\n")
    if(file_type=='csv'):
        utility.updateZoneFromCSV(input_file,zone_id)
    else:
        utility.updateZoneFromJSON(input_file,zone_id)
else:
    zone_id=input("Enter zone ID \n")
    if(file_type=='csv'):
        utility.updateZonePolicyFromCSV(input_file,zone_id)
    else:
        utility.updateZonePolicyFromJSON(input_file,zone_id)

